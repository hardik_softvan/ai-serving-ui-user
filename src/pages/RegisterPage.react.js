import React, { Fragment, Component } from "react";
import { Form, Card, Button, FormGroup, Checkbox } from "tabler-react";
class RegisterPage extends React.Component {

  constructor() {
    super();
    this.state = {
      registerForm: {
        firstName: '',
        lastName: '',
        email: '',
        password: ''
      },
      errors: {},
    }
  }

  handleChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    let registerDetails = { ...this.state.registerForm }
    registerDetails[name] = value;
    this.setState({
      registerForm: registerDetails,
    })
  }

  handleSubmit = (event) => {
    event.preventDefault();

    
    if (this.validate()) {

      let registerForm = {};
      registerForm["firstName"] = "";
      registerForm["lastName"] = "";
      registerForm["email"] = "";
      registerForm["password"] = "";
      this.setState({
        registerForm: registerForm
      });

      console.log({ ...this.state.registerForm });
    }

  }

  //Validation Method
  validate = () => {
    let registerForm = { ...this.state.registerForm };


    let errors = {};
    let isValid = true;

    if (registerForm["firstName"] === "") {
      isValid = false;
      errors["firstName"] = "*Please Enter First Name";


    }
    if (registerForm["lastName"] === "") {
      isValid = false;
      errors["lastName"] = "*Please Enter Last Name";


    }
    if (registerForm["email"] === "") {
      isValid = false;
      errors["email"] = "*Please Enter Email";


    } else {

      if (typeof registerForm["email"] !== "undefined") {
        //regular expression for email validation
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        if (!registerForm["email"].match(pattern)) {
          isValid = false;
          errors["email"] = "*Please Enter Valid Email Address";
        }
      }
    }


    if (registerForm["password"] === "") {
      isValid = false;
      errors["password"] = "*Please Enter Password";


    } else {
      if (typeof registerForm["password"] !== "undefined") {
        //regular expression for password validation
        var pattern = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)
        if (!registerForm["password"].match(pattern)) {
          isValid = false;
          errors["password"] = "*Minimum eight characters, at least one letter and one number";
        }
      }
    }

    this.setState({
      errors: errors
    });

    return isValid;
  }

  render() {

    return (

      <Fragment>


        <div className="page">
          <div className="page-single">
            <div className="container">
              <div className="row">
                <div className="col col-login mx-auto">
                  <div className="text-center mb-6">
                    <img src={"./demo/logo.svg"} className="h-6" alt="logo" />
                  </div>
                  <Form className="card" onSubmit={this.handleSubmit} method="post">
                    <Card.Body className="p-6">
                      <Card.Title RootComponent="div">Create New Account</Card.Title>
                      <Form.Group>
                        <Form.Label>First Name</Form.Label>
                        <Form.Input type="text" name="firstName" value={this.state.registerForm.firstName} placeholder="Enter First Name" onChange={this.handleChange}></Form.Input>
                        <div className="text-danger">{this.state.errors.firstName}</div>
                      </Form.Group>
                      <Form.Group>
                        <Form.Label>Last Name</Form.Label>
                        <Form.Input type="text" name="lastName" value={this.state.registerForm.lastName} placeholder="Enter Last Name" onChange={this.handleChange}></Form.Input>
                        <div className="text-danger">{this.state.errors.lastName}</div>
                      </Form.Group>
                      <Form.Group>
                        <Form.Label>Email Address</Form.Label>
                        <Form.Input type="text" name="email" value={this.state.registerForm.email} placeholder="Enter Email" onChange={this.handleChange}></Form.Input>
                        <div className="text-danger">{this.state.errors.email}</div>
                      </Form.Group>
                      <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Input type="password" name="password" value={this.state.registerForm.password} placeholder="Enter Password" onChange={this.handleChange}></Form.Input>
                        <div className="text-danger">{this.state.errors.password}</div>
                      </Form.Group>
                      <Form.Group>
                        <input type="checkbox" name="checkbox" />
                        <label>&nbsp; Agree to the terms and policy</label>

                      </Form.Group>

                      <Form.Footer>
                        <Button type="submit" color="primary" block={true}>
                          Create Account
                        </Button>
                      </Form.Footer>
                    </Card.Body>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>


      </Fragment>

    )

  }


}

export default RegisterPage;
