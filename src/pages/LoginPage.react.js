import React, { Fragment, Component } from "react";
import { Form, Card, Button, FormGroup, Checkbox } from "tabler-react";
import { loginAPI, loginTestAPI } from "../service/DataService";
import Cookies from 'universal-cookie';
class LoginPage extends React.Component {

  constructor() {
    super();
    this.state = {
      loginForm: {
        username: '',
        password: ''
      },
      errors: {},
      userRole: '',
    };

  }


  login = (data) => {
    let formData = new FormData();
    formData.append("username", data.username)
    formData.append("password", data.password)
console.log("form::",formData);
    loginAPI(formData).then(response => {

      if (response) {
        let token = response.access_token;
        this.setCookie(token);

      }

    });
  };




  setCookie = (token) => {
    const cookies = new Cookies();
    cookies.set('access_token', token, { httpOnly: false });
    this.loginTest();
  };

  loginTest = () => {
    loginTestAPI().then(response => {
      if (response) {
        let userRoles = response.roles[0];
        {

          if (userRoles.role === "superadmin") {

            window.location.href = "http://localhost:3001/employee";
          };

        }



      };

    });

  };




  handleChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;

    let loginDetails = { ...this.state.loginForm }
    loginDetails[name] = value;

    this.setState({
      loginForm: loginDetails,
    })
  }

  handleSubmit = (event) => {
    event.preventDefault();



    this.login({ ...this.state.loginForm });
    // if (this.validate()) {
    //   let loginForm = {};
    //   loginForm["username"] = "";
    //   loginForm["password"] = "";

    //   this.setState({
    //     loginForm: loginForm,
    //   });



    // }

  }

  //Validation Method
  // validate = () => {

  //   let loginForm = { ...this.state.loginForm };
  //   let isValid = true;
  //   let errors = {};

  //   if (loginForm["username"] === "") {
  //     isValid = false;
  //     errors["username"] = "*Please Enter Email Address"
  //   } else {
  //     if (loginForm["username"] !== undefined) {
  //       var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

  //       if (!loginForm["username"].match(pattern)) {
  //         isValid = false;
  //         errors["username"] = "*Please Enter Valid Email Address"
  //       }
  //     }

  //   }

  //   if (loginForm["password"] === "") {
  //     isValid = false;
  //     errors["password"] = "*Please Enter Password";
  //   }

  //   // } else {
  //   //   if (typeof loginForm["password"] !== "undefined") {
  //   //     //regular expression for password validation
  //   //     var pattern = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)
  //   //     if (!loginForm["password"].match(pattern)) {
  //   //       isValid = false;
  //   //       errors["password"] = "*Minimum eight characters, including one letter and one number";
  //   //     }
  //   //   }
  //   // }


  //   this.setState({
  //     errors: errors
  //   })

  //   return isValid;
  // }

  render() {

    return (

      <Fragment>


        <div className="page">
          <div className="page-single">
            <div className="container">
              <div className="row">
                <div className="col col-login mx-auto">
                  <div className="text-center mb-6">
                    <img src={"./demo/logo.svg"} className="h-6" alt="logo" />
                  </div>

                  <Form className="card" onSubmit={this.handleSubmit} method="post">
                    <Card.Body className="p-6">
                      <Card.Title RootComponent="div">Login to your Account</Card.Title>

                      <Form.Group>
                        <Form.Label>Email Address</Form.Label>
                        <Form.Input type="text" name="username" value={this.state.loginForm.username} placeholder="Enter Email" onChange={this.handleChange}></Form.Input>
                        <div className="text-danger">{this.state.errors.username}</div>
                      </Form.Group>
                      <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Input type="password" name="password" value={this.state.loginForm.password} placeholder="Enter Password" onChange={this.handleChange}></Form.Input>
                        <div className="text-danger">{this.state.errors.password}</div>
                      </Form.Group>


                      <Form.Footer>
                        <Button type="submit" color="primary" block={true}>
                          Login
                        </Button>
                      </Form.Footer>
                    </Card.Body>
                  </Form>

                </div>
              </div>
            </div>
          </div>
        </div>


      </Fragment>

    )

  }


}

export default LoginPage;
