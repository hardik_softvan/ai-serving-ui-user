import {request} from './UtilService';
import {BASE_URL} from '../constant/APIs';

export async function loginAPI(data){
   
    
    return await request({
        url: BASE_URL + "/login/access-token",
        method: 'POST',
        body: data
    }); 

}

export async function loginTestAPI(){
   
    
    return await request({
        url: BASE_URL + "/login/test-token",
        method: 'POST',
       
    }); 

}