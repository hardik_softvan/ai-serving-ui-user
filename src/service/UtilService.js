import Cookies from 'universal-cookie';
const axios = require('axios');

export const request = (options) => {

    const config = {
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        url: options['url'],
        method: options['method'],
        data: options['body']
    };


    const cookies = new Cookies();
    if (cookies.get('access_token')) {
        config['headers']['Authorization'] = 'Bearer ' + cookies.get('access_token', {httpOnly: false});
    };

  

    return axios.request(config)
        .then(response => {
          
            return response.data;
        })
        .catch((error) => {
               
            if(error.request.status == 401){
                
              // this.props.history.push("/login");
               window.location.href ="http://localhost:3000/login";
            } 

            return error;
        })
};

